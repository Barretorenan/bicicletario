//package com.example.aluguel.service;
//
//import com.example.echo.aluguel.model.Ciclista;
//import com.example.echo.aluguel.model.Cobranca;
//import com.example.echo.aluguel.model.Email;
//import com.example.echo.aluguel.model.MeioDePagamento;
//import com.example.echo.aluguel.service.EmailService;
//import org.apache.commons.mail.EmailException;
//import org.apache.commons.mail.SimpleEmail;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//
//import static org.junit.jupiter.api.Assertions.assertFalse;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//import static org.mockito.Mockito.*;
//
//class EmailServiceTest {
//
//    @Mock
//    private SimpleEmail simpleEmailMock;
//
//    @InjectMocks
//    private EmailService emailService;
//
//    @BeforeEach
//    void setUp() {
//        MockitoAnnotations.openMocks(this);
//    }
//
//
//    @Test
//    void enviarEmail_ShouldReturnTrue() throws EmailException {
//        // Arrange
//        Email email = new Email("test@example.com", "Test Email", "This is a test email.");
//
//        // Act
//        Boolean result = emailService.enviarEmail(email);
//
//        Assertions.assertTrue(result);
//    }
//
//    @Test
//    void isValidEmail_ShouldReturnTrueForValidEmail() {
//        // Arrange
//        String validEmail = "test@example.com";
//
//        // Act
//        boolean result = emailService.isValidEmail(validEmail);
//
//        // Assert
//        assertTrue(result);
//    }
//
//    @Test
//    void isValidEmail_ShouldReturnFalseForInvalidEmail() {
//        // Arrange
//        String invalidEmail = "invalid.email";
//
//        // Act
//        boolean result = emailService.isValidEmail(invalidEmail);
//
//        // Assert
//        assertFalse(result);
//    }
//}