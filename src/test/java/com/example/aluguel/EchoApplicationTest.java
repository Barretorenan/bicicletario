package com.example.aluguel;

import com.example.echo.EchoApplication;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class EchoApplicationTest {
    @Test
    public void mainTest() {
        Assertions.assertDoesNotThrow(() -> EchoApplication.main(new String[]{}));
    }
    @Test
    public void contextLoads() {
        // Esse teste verifica se o contexto da aplicação é carregado corretamente
        // Se não houver erros durante a inicialização da aplicação, o teste será considerado bem-sucedido
        Assertions.assertTrue(true);
    }
}